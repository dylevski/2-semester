#include "arrays.h"
#include "iostream"

int* WorkingWithArrays::allocateMemory(int n)
{
	if (n <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	int* p = new int[n];
	return p;
}

bool* WorkingWithArrays::allocateMemoryBoolean(int n)
{
	if (n <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	bool* p = new bool[n];
	return p;
}

int** WorkingWithArrays::allocateMemory(int width, int heigth)
{
	if (heigth <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	int** matrix = new int* [heigth];
	for (size_t i = 0; i < heigth; i++)
	{
		matrix[i] = WorkingWithArrays::allocateMemory(width);
	}
	return matrix;
}

void WorkingWithArrays::randomValues(int* p, int n)
{
	if (n <= 0)
	{
		return;
	}
	srand(time(nullptr));
	for (int* q = p; q < p + n; q++)
	{
		*q = rand() % 1000-500;
	}
}

void WorkingWithArrays::randomValues(int** matrix, int width, int heigth)
{
	if (width <= 0 || heigth <=0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (matrix == nullptr)
	{
		throw std::invalid_argument("nullptr pointer");
	}
	srand(time(nullptr));
	for (size_t i = 0; i < heigth; i++)
	{
		WorkingWithArrays::randomValues(matrix[i], width);
	}
}

void WorkingWithArrays::displayArray(int* p, int n)
{
	if (n <= 0)
	{
		return;
	}
	for (int* q = p; q < p + n; q++)
	{
		std::cout << *q;
		std::cout << " ";
	}
}

void WorkingWithArrays::resizeArray(int*& p, int oldSize, int newSize)
{
	int* q = WorkingWithArrays::allocateMemory(newSize);
	int minSize = oldSize < newSize ? oldSize : newSize;
	for (int* r = p; r < p + minSize; r++, q++)
	{
		*q = *r;
	}
	delete[]p;
	p = q - minSize;
}

void WorkingWithArrays::extendArray(int*& p, int oldSize, int newSize)
{
	if (newSize<oldSize)
	{
		throw std::invalid_argument("New size must be larger than old.");
	}
	if (newSize == oldSize)
	{
		return;
	}
	int* q = WorkingWithArrays::allocateMemory(newSize);
	for (int* r = p; r < p + oldSize; r++, q++)
	{
		*q = *r;
	}
	delete[]p;
	p = q - oldSize;
}

void WorkingWithArrays::addOneToEnd(int*& p, int& size, int value)
{
	WorkingWithArrays::resizeArray(p, size, size++);
	*(p + size - 1) = value;
}

void WorkingWithArrays::delOneFromEnd(int*& p, int& size)
{
	WorkingWithArrays::resizeArray(p, size, size--);
}

void WorkingWithArrays::addOneToBegin(int*& p, int& size, int value)
{
	WorkingWithArrays::addOneToEnd(p, size, 0);
	for (int i = size; i > 0; i--)
	{
		*(p + size - i - 1) = *(p + size - i);
	}
	*p = value;
}

void WorkingWithArrays::delOneFromBegin(int*& p, int& size)
{
	for (int i = 1; i < size; i++)
	{
		*(p + i) = *(p + i + 1);
	}
	WorkingWithArrays::delOneFromEnd(p, size);
}

void WorkingWithArrays::addOneToCertainPos(int*& p, int& size, int index, int value)
{
	WorkingWithArrays::addOneToEnd(p, size, 0);
	for (int i = size; i > index; i--)
	{
		*(p + i) = *(p + i - 1);
	}
	*(p + index) = value;
}

void WorkingWithArrays::delOneFromCertainPos(int*& p, int& size, int index)
{
	for (int i = index + 1; i < size; i++)
	{
		*(p + i) = *(p + i + 1);
	}
	WorkingWithArrays::delOneFromEnd(p, size);
}

int WorkingWithArrays::sumOfElements(int* p, int size)
{
	int sumValue = 0;
	for (size_t i = 0; i < size; i++)
	{
		sumValue += *(p + i);
	}
	return sumValue;
}

double WorkingWithArrays::averageValue(int* p, int size)
{
	return static_cast<double>(WorkingWithArrays::sumOfElements(p, size)) / size;
}

int WorkingWithArrays::indexOfMinElement(int* p, int size)
{
	int minIndex = 0;
	for (size_t i = 1; i < size; i++)
	{
		if (minIndex < *(p+i))
		{
			minIndex = i;
		}
	}
	return minIndex;
}

int WorkingWithArrays::indexOfMaxElement(int* p, int size)
{
	int maxIndex = 0;
	for (size_t i = 1; i < size; i++)
	{
		if (maxIndex > *(p + i))
		{
			maxIndex = i;
		}
	}
	return maxIndex;
}

int* WorkingWithArrays::filterByBooleanArray(int* p, int length, int& newLength, bool* isValued)
{
	newLength = 0;
	int* result = allocateMemory(length);
	for (size_t i = 0, k = 0; i< length; i++)
	{
		if (isValued[i])
		{
			result[k] = p[i];
			k++;
			newLength++;
		}
	}
	if (newLength != length)
	{
		WorkingWithArrays::resizeArray(result, length, newLength);
	}
	return result;
}

int* WorkingWithArrays::filterByPredicate(int* p, int length, int& newLength, predicate func)
{
	newLength = 0;
	int* result = allocateMemory(length);
	for (int* r = p, *q = result; r < p + length; r++)
	{
		if (func(*r))
		{
			*q = *r;
			q++;
			newLength++;
		}
	}
	if (newLength != length)
	{
		WorkingWithArrays::resizeArray(result, length, newLength);
	}
	return result;
}

int* WorkingWithArrays::filterByPredicateArr(int* p, int length, int& newLength, predicateArr func)
{
	newLength = 0;
	int* result = allocateMemory(length);
	for (size_t i = 0, k = 0; i < length; i++)
	{
		if (func(p, i))
		{
			result[k] = p[i];
			k++;
			newLength++;
		}
	}
	if (newLength != length)
	{
		WorkingWithArrays::resizeArray(result, length, newLength);
	}
	return result;
}


double* WorkingWithArrays::transformArray(int* p, int size)
{
	double* q = new double[size];
	for (int i = 0; i < size; i++)
	{
		double x = *(p + i);
		*(q + i) = sin(x)+cos(x);
	}
	return q;
}

int** WorkingWithArrays::lowTriangleMatrix(int size)
{
	if (size <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	int** matrix = new int* [size];
	for (size_t i = 0; i < size; i++)
	{
		int k = i + 1;
		matrix[i] = WorkingWithArrays::allocateMemory(k);
		srand(time(nullptr));
		WorkingWithArrays::randomValues(matrix[i], k);
	}
	return matrix;
}

int** WorkingWithArrays::highTriangleMatrix(int size)
{
	if (size <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	int** matrix = new int* [size];
	for (size_t i = 0; i < size; i++)
	{
		int k = i + 1;
		matrix[i] = WorkingWithArrays::allocateMemory(size-i);
		srand(time(nullptr));
		WorkingWithArrays::randomValues(matrix[i], size-i);
	}
	return matrix;
}

int** WorkingWithArrays::sumOfHighAndLowMatrices(int** highMatrix, int** lowMatrix, int size)
{
	if (size <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (highMatrix == nullptr || lowMatrix == nullptr)
	{
		throw std::invalid_argument("nullptr pointer.");
	}
	int** sum = new int* [size];
	for (size_t i = 0; i < size; i++)
	{
		bool isAfterDiag = false;
		sum[i] = WorkingWithArrays::allocateMemory(size);
		for (size_t j = 0; j < size; j++)
		{
			if (i == j)
			{
				sum[i][j] = highMatrix[i][0] + lowMatrix[i][j];
				isAfterDiag = true;
				continue;
			}
			sum[i][j] = isAfterDiag ? highMatrix[i][j-i] : lowMatrix[i][j];
		}
	}
	return sum;
}

void WorkingWithArrays::displayMatrix(int** matrix, int width, int heigth)
{
	if (width <= 0 || heigth <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (matrix == nullptr)
	{
		throw std::invalid_argument("nullptr pointer.");
	}
	for (size_t i = 0; i < heigth; i++)
	{
		WorkingWithArrays::displayArray(matrix[i], width);
		std::cout << std::endl;
	}
}

void WorkingWithArrays::displayMatrix(int** matrix, int size)
{
	if (size <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (matrix == nullptr)
	{
		throw std::invalid_argument("nullptr pointer.");
	}
	for (size_t i = 0; i < size; i++)
	{
		WorkingWithArrays::displayArray(matrix[i], size);
		std::cout << std::endl;
	}
}

void WorkingWithArrays::inputMatrix(int** matrix, int width, int heigth)
{
	if (width <= 0 || heigth <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (matrix == nullptr)
	{
		throw std::invalid_argument("nullptr pointer");
	}
	for (size_t i = 0; i < heigth; i++)
	{
		for (size_t j = 0; j < width; j++)
		{
			system("cls");
			std::cin >> matrix[i][j];
		}
	}
}

void WorkingWithArrays::displayMatrixLow(int** matrix, int size)
{
	if (size <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (matrix == nullptr)
	{
		throw std::invalid_argument("nullptr pointer.");
	}
	for (size_t i = 0; i < size; i++)
	{
		WorkingWithArrays::displayArray(matrix[i], i+1);
		for (size_t j = i+1; j < size; j++)
		{
			std::cout.width(5);
			std::cout << 0;
		}
		std::cout << std::endl;
	}
}

void WorkingWithArrays::displayMatrixHigh(int** matrix, int size)
{
	if (size <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (matrix == nullptr)
	{
		throw std::invalid_argument("nullptr pointer.");
	}
	{
		for (size_t i = 0; i < size; i++)
		{
			for (size_t j = 0; j < i; j++)
			{
				std::cout.width(5);
				std::cout << 0;
			}

			WorkingWithArrays::displayArray(matrix[i], size - i);

			std::cout << std::endl;
		}
	}
}

int** WorkingWithArrays::convertToHighMatrix(int** lowMatrix, int size)
{
	if (size <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (lowMatrix==nullptr)
	{
		throw std::invalid_argument("nullptr pointer");
	}
	int** copy = new int* [size];
	for (size_t i = 0; i < size; i++)
	{
		copy[i]=WorkingWithArrays::allocateMemory(size-i);
		for (size_t j = 0; j < size-i; j++)
		{
			copy[i][j] = lowMatrix[j+i][i];
		}
	}
	return copy;
}

int* WorkingWithArrays::convertTo(int** source, int width, int heigth)
{
	int* arr = WorkingWithArrays::allocateMemory(width*heigth);
	for (size_t i = 0; i < heigth;i++)
	{
		for (size_t j = 0; j < width; j++)
		{
			int k = i * heigth + j;
			arr[k] = source[i][j];
		}
	}
	return arr;
}

int** WorkingWithArrays::convertTo(int* source, int width, int heigth)
{
	if (width <= 0 || heigth <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (source == nullptr)
	{
		throw std::invalid_argument("nullptr pointer.");
	}
	int** matrix = WorkingWithArrays::allocateMemory(width, heigth);
	for (size_t i = 0; i < heigth; i++)
	{
		for (size_t j = 0; j < width; j++)
		{
			int k = i * heigth + j;
			matrix[i][j] = source[k];
		}
	}
	return matrix;
}

void WorkingWithArrays::bubbleSortRowsByComparer(int** matrix, int width, int heigth, comparerArr func)
{
	if (width <= 0 || heigth <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (matrix == nullptr)
	{
		throw std::invalid_argument("nullptr pointer.");
	}
	for (int i = 0; i < heigth; i++)
	{
		for (int j = 0; j < heigth - i - 1; j++)
		{
			if (func(matrix[j], matrix[j+1], width))
			{
				WorkingWithArrays::swap(matrix[j], matrix[j+1]);
			}
		}
	}
}

void WorkingWithArrays::bubbleSortByKey(int* p, int size, key key)
{
	if (size <= 0)
	{
		throw std::out_of_range("Count of elements must be positive.");
	}
	if (p == nullptr)
	{
		throw std::invalid_argument("nullptr pointer.");
	}
	int* keys = WorkingWithArrays::allocateMemory(size);
	for (size_t i = 0; i < size; i++)
	{
		keys[i] = key(p[i]);
	}

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (keys[j] > keys[j+1])
			{
				swap(p[j], p[j+1]);
				swap(keys[j], keys[j+1]);
			}
		}
	}
}

void WorkingWithArrays::swap(int& a, int& b)
{
	int tmp = a;
	a = b;
	b = tmp;
}

void WorkingWithArrays::swap(int*& a, int*& b)
{
	int* tmp = a;
	a = b;
	b = tmp;
}

bool WorkingWithArrays::isEven(int number)
{
	return !(number % 2);
}

bool WorkingWithArrays::isOdd(int number)
{
	return number % 2;
}

bool WorkingWithArrays::isPositive(int number)
{
	return number > 0;
}
