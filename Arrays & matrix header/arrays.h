#pragma once
namespace WorkingWithArrays
{
	using predicate = bool(*)(int);
	using predicateArr = bool(*)(int*, int);
	using key = int(*)(int);
	using comparerArr = bool(*)(int*, int*, int);

	int* allocateMemory(int);
	bool* allocateMemoryBoolean(int);
	int** allocateMemory(int, int);
	void randomValues(int*, int);
	void randomValues(int**, int, int);
	void displayArray(int*, int);
	void resizeArray(int*&, int, int);
	void extendArray(int*&, int, int);
	void addOneToEnd(int*&, int&, int);
	void delOneFromEnd(int*&, int&);
	void addOneToBegin(int*&, int&, int);
	void delOneFromBegin(int*&, int&);
	void addOneToCertainPos(int*&, int&, int, int);
	void delOneFromCertainPos(int*&, int&, int);
	int sumOfElements(int*, int);
	double averageValue(int*, int);
	int indexOfMinElement(int*, int);
	int indexOfMaxElement(int*, int);
	int* filterByBooleanArray(int*, int, int&, bool*);
	int* filterByPredicate(int*, int, int&, predicate);
	int* filterByPredicateArr(int*, int, int&, predicateArr);
	double* transformArray(int*, int);
	int** lowTriangleMatrix(int);
	int** highTriangleMatrix(int);
	int** sumOfHighAndLowMatrices(int**, int**, int);
	void displayMatrix(int**, int, int);
	void displayMatrix(int**, int);
	void inputMatrix(int**, int, int);
	void displayMatrixLow(int**, int);
	void displayMatrixHigh(int**, int);
	int** convertToHighMatrix(int**, int);
	int* convertTo(int**, int, int);
	int** convertTo(int*, int, int);
	void bubbleSortRowsByComparer(int**, int, int, comparerArr);
	void bubbleSortByKey(int*, int, key);
	void swap(int&, int&);
	void swap(int*&, int*&);
	bool isEven(int);
	bool isOdd(int);
	bool isPositive(int);
}