#define _CRT_SECURE_NO_WARNINGS
#pragma once
namespace WorkingWithStrings
{
	void swap(char&, char&);
	void displayArray(char[]);
	int lengthArray(char[]);
	int firstEntrance(char[], char[]);
	void deleteSubstring(char*, char*);
}