#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "strings.h"

void WorkingWithStrings::swap(char& a, char& b)
{
    char tmp = a;
    a = b;
    b = tmp;
}

void WorkingWithStrings::displayArray(char arr[])
{
    int length = WorkingWithStrings::lengthArray(arr);
    for (int i = 0; i < length; i++)
    {
        std::cout << arr[i];
    }
}

int WorkingWithStrings::lengthArray(char arr[])
{
    int i = 0;
    for (; arr[i]; i++)
    {

    }
    return i;
}

int WorkingWithStrings::firstEntrance(char string[], char substring[])
{
    int entranceIndex = 0;
    bool isEnter = false;
    for (int i = 0; string[i]; i++)
    {
        for (int j = 0; substring[j]; j++)
        {
            if (substring[j] == string[i + j])
            {
                isEnter = true;
                entranceIndex = i;
                if (!substring[j + 1])
                {
                    return entranceIndex;
                }
            }
            else
            {
                isEnter = false;
                entranceIndex = 0;
                break;
            }
        }
    }

    return -1;
}

void WorkingWithStrings::deleteSubstring(char* string, char* substring)
{
    int subLength = WorkingWithStrings::lengthArray(substring);
    char* p = strstr(string, substring);
    while (p)
    {
        strcpy(p, p+subLength);
        p = strstr(p, substring);
    }
}