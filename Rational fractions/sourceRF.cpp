﻿#include <iostream>
#include <string>
#include "RationalFraction.h"

int main()
{
    RationalFraction frac1(8, 6);
    cout << "Frac1: " << frac1.toString() << endl;
    frac1.simplify();
    cout << "Frac1: " << frac1.toString() << endl;
    RationalFraction frac2(2, 5);
    cout << "Frac2: " << frac2.toString() << endl;
    RationalFraction frac3 = frac1 + frac2;
    cout << "Frac1 + Frac2: " << frac3.toString() << endl;
    frac3 = frac3 * frac2;
    cout << "...* Frac2: " << frac3.toString() << endl;
    frac3 = frac3 / frac1;
    cout << ".../ Frac1: " << frac3.toString() << endl;
    frac3 = frac1 * 3;
    cout << "Frac1 * 3: " << frac3.toString() << endl;
    frac3 = frac1 / 2;
    cout << "Frac1 / 2: " << frac3.toString() << endl;
    frac3 = frac2 + 3;
    cout << "Frac2 + 3: " << frac3.toString() << endl;
    cout << "Frac3 denominator: " << frac3.getDenominator() << endl;
    frac3.setNumerator(228);
    cout << "Frac3 new numerator: " << frac3.getNumerator() << endl;
}

