#include "RationalFraction.h"
#include "iostream"
#include <cmath>

RationalFraction::RationalFraction() : RationalFraction(1, 1)
{

}

RationalFraction::RationalFraction(int numerator, int denominator)
{
	if (denominator<0)
	{
		numerator = -numerator;
		denominator = -denominator;
	}
	if (denominator == 0)
	{
		throw std::invalid_argument("Not allowed argument");
	}
	this->numerator = numerator;
	this->denominator = denominator;
}

void RationalFraction::simplify()
{
	int GCD = getGCD(this->numerator, this->denominator);
	this->numerator /= GCD;
	this->denominator /= GCD;
}

string RationalFraction::toString()
{
	return to_string(this->numerator)+"/"+to_string(this->denominator);
}

double RationalFraction::absoluteValue()
{
	return static_cast<double>(this->numerator)/this->denominator;
}

RationalFraction RationalFraction::add(RationalFraction fraction1, RationalFraction fraction2)
{
	RationalFraction tmpFrac1 = fraction1.toCD(fraction2), tmpFrac2 = fraction2.toCD(fraction1);
	RationalFraction result(tmpFrac1.numerator + tmpFrac2.numerator, tmpFrac1.denominator);
	result.simplify();
	return result;
}

RationalFraction RationalFraction::add(RationalFraction fraction, int num)
{
	return add(fraction, RationalFraction(num, 1));
}

RationalFraction RationalFraction::subtract(RationalFraction fraction1, RationalFraction fraction2)
{
	RationalFraction tmpFrac1 = fraction1.toCD(fraction2), tmpFrac2 = fraction2.toCD(fraction1);
	RationalFraction result(tmpFrac1.numerator - tmpFrac2.numerator, tmpFrac1.denominator);
	result.simplify();
	return result;
}

RationalFraction RationalFraction::subtract(RationalFraction fraction, int num)
{
	return subtract(fraction, RationalFraction(num, 1));
}

RationalFraction RationalFraction::mul(RationalFraction fraction1, RationalFraction fraction2)
{
	RationalFraction result(fraction1.numerator * fraction2.numerator, fraction1.denominator * fraction2.denominator);
	result.simplify();
	return result;
}

RationalFraction RationalFraction::mul(RationalFraction fraction, int num)
{
	return mul(fraction, RationalFraction(num, 1));
}

RationalFraction RationalFraction::divide(RationalFraction fraction1, RationalFraction fraction2)
{
	if (fraction2.numerator == 0)
	{
		throw std::invalid_argument("Not allowed argument");
	}
	RationalFraction result(fraction1.numerator * fraction2.denominator, fraction1.denominator * fraction2.numerator);
	result.simplify();
	return result;
}

RationalFraction RationalFraction::divide(RationalFraction fraction, int num)
{
	return divide(fraction, RationalFraction(num, 1));
}

RationalFraction RationalFraction::power(RationalFraction fraction, int num)
{
	return RationalFraction(pow(fraction.numerator, num), pow(fraction.denominator, num));
}




RationalFraction RationalFraction::operator+(const RationalFraction& frac)
{
	return RationalFraction::add(*this, frac);
}

RationalFraction RationalFraction::operator-(const RationalFraction& frac)
{
	return RationalFraction::subtract(*this, frac);
}

RationalFraction RationalFraction::operator*(const RationalFraction& frac)
{
	return RationalFraction::mul(*this, frac);
}

RationalFraction RationalFraction::operator/(const RationalFraction& frac)
{
	return RationalFraction::divide(*this, frac);
}



RationalFraction RationalFraction::operator+(const int number)
{
	return RationalFraction::add(*this, number);
}

RationalFraction RationalFraction::operator-(const int number)
{
	return RationalFraction::subtract(*this, number);
}

RationalFraction RationalFraction::operator*(const int number)
{
	return RationalFraction::mul(*this, number);
}

RationalFraction RationalFraction::operator/(const int number)
{
	return RationalFraction::divide(*this, number);
}

int RationalFraction::getNumerator() {
	return this->numerator;
}

int RationalFraction::getDenominator() {
	return this->denominator;
}

void RationalFraction::setNumerator(int value)
{
	this->numerator = value;
}

void RationalFraction::setDenominator(int value)
{
	this->denominator = value;
}

int RationalFraction::getGCD(int num1, int num2)
{
	num1 = abs(num1);
	num2 = abs(num2);
	int min = num2;
	if (num1<num2)
	{
		min = num1;
	}
	for (size_t i = min; i > 0; i--)
	{
		if (num1 % i == 0 && num2 % i == 0)
		{
			return i;
		}
	}
	return 0;
}

RationalFraction RationalFraction::toCD(RationalFraction frac)
{
	return RationalFraction(this->numerator * frac.denominator, this->denominator * frac.denominator;
}
