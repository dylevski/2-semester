#pragma once
#include <string>
using namespace std;
class RationalFraction
{
public:
	RationalFraction();
	RationalFraction(int numerator, int denominator);
	void simplify();
	string toString();
	double absoluteValue();
	static RationalFraction add(RationalFraction fraction1, RationalFraction fraction2);
	static RationalFraction add(RationalFraction fraction, int num);
	static RationalFraction subtract(RationalFraction fraction1, RationalFraction fraction2);
	static RationalFraction subtract(RationalFraction fraction, int num);
	static RationalFraction mul(RationalFraction fraction1, RationalFraction fraction2);
	static RationalFraction mul(RationalFraction fraction, int num);
	static RationalFraction divide(RationalFraction fraction1, RationalFraction fraction2);
	static RationalFraction divide(RationalFraction fraction, int num);
	static RationalFraction power(RationalFraction fraction, int num);
	RationalFraction operator+(const RationalFraction& frac);
	RationalFraction operator-(const RationalFraction& frac);
	RationalFraction operator*(const RationalFraction& frac);
	RationalFraction operator/(const RationalFraction& frac);
	RationalFraction operator+(const int number);
	RationalFraction operator-(const int number);
	RationalFraction operator*(const int number);
	RationalFraction operator/(const int number);
	int getNumerator();
	int getDenominator();
	void setNumerator(int value);
	void setDenominator(int value);
private:
	int numerator;
	int denominator;
	int getGCD(int num1, int num2);

	RationalFraction toCD(RationalFraction frac);
};
