#include <iostream>
#include "algorithms.h"

void swap(int& a, int& b)
{
    int tmp = a;
    a = b;
    b = tmp;
}

void bubbleSort(int numbers[], int length)
{
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length-i-1; j++)
        {
            if (numbers[j]>numbers[j+1])
            {
                swap(numbers[j], numbers[j+1]);
            }
        }
    }
}

void selectionSort(int numbers[], int length)
{
    int currentMinIndex;
    for (int i = 0; i < length; i++)
    {
        currentMinIndex = i;
        for (int j = i+1; j < length; j++)
        {
            if (numbers[currentMinIndex]>numbers[j])
            {
                currentMinIndex = j;
            }
        }
        swap(numbers[currentMinIndex], numbers[i]);
    }
}

void insertionSort(int numbers[], int length)
{
    for (int i = 0; i < length-1; i++)
    {
        for (int j = i; numbers[j] > numbers[j+1] && j>=0; j--)
        {
            swap(numbers[j], numbers[j + 1]);
        }
    }
}

void displayArray(int arr[], int length)
{
    for (int i = 0; i < length; i++)
    {
        std::cout << arr[i] << " ";
    }
}

int binarySearch(int arr[], int searchedElement, int length)
{
    int firstIndex = 0;
    int lastIndex = length - 1;

    while (firstIndex < lastIndex)
    {
        int middleIndex = (lastIndex + firstIndex) / 2;

        if (searchedElement < arr[middleIndex])
        {
            lastIndex = middleIndex - 1;
        }
        else if (searchedElement > arr[middleIndex])
        {
            firstIndex = middleIndex + 1;
        }
        else
        {
            return middleIndex;
        }
    }

    if (firstIndex == lastIndex)
    {
        if (arr[lastIndex] == searchedElement)
        {
            return lastIndex;
        }
        else
        {
            return -1;
        }
    }
}

void randomArray(int numbers[], int length, int max)
{
    srand(time(nullptr));
    for (int i = 0; i < length; i++)
    {
        numbers[i] = rand() % max;
    }
}