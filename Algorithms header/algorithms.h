#pragma once
	void swap(int&, int&);
	void bubbleSort(int [], int);
	void selectionSort(int [], int);
	void insertionSort(int[], int);
	void displayArray(int [], int);
	int binarySearch(int [], int , int);
	void randomArray(int[], int, int);